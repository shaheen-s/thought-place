import React from "react"
import ReactDOM from "react-dom"
import {Route, BrowserRouter as Router} from "react-router-dom"
import App from "./App.js"
import AppCss from "./App.css"
import SignIn from "./components/pages/SignIn"
import SignUp from "./components/pages/SignUp"


const routing = (
  <Router>
    <div>
      <Route exact path="/" component={App} />
      <Route exact path="/signin" component={SignIn} />
      <Route exact path="/signup" component={SignUp} />
    </div>
  </Router>
)

ReactDOM.render(routing, document.getElementById('root'));
