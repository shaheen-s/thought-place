import React, {useState} from "react";
import Thought from "./Thought";

export default (props) => {
  const thoughts = props.thoughts;
  if(!thoughts) return (<div> </div>)

  const jsxThoughts = [];

  for(let thought of thoughts){
    jsxThoughts.push(
      <Thought user={thought.user} text={thought.text} />
    )
  }

  return (
    <div className="CenteredDiv">
      {jsxThoughts}
    </div>
  )
}
