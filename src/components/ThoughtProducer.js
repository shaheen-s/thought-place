import React from "react";


export default (props) => {
  return (
    <div className="ThoughtProducer">
      <form action="../php/think.php" method="POST">
        <input type="text" disabled name="username" value={props.user} />
        <textarea name="thoughtProducer"> </textarea>
        <input type="submit" value="Think!" />
      </form>
    </div>
  )
}
