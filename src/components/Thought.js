import React from "react";


export default (props) => {
  const profSource = `profilepic${Math.floor( Math.random() * 10 % 3 + 1 )}.jpg`;

  return (
    <div className="Thought">
      <header className="ThoughtUserDisplay">
        <img src={profSource} className="ProfilePicture" alt="pp" />
        <h4> {props.user} </h4>
      </header>
        <p> {props.text} </p>
    </div>
  )
}
