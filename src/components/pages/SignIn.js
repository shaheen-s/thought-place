import React, { useState } from "react";
import { Redirect } from "react-router-dom";


export default (props) => {
  const [loggedIn, updateLoggedIn] = useState(false);
  const signInRq = () => {
    const xmlHttp =  new XMLHttpRequest();


    xmlHttp.onload = function (param1, event) {
      console.log(this.response)
      if(JSON.parse(this.response)) updateLoggedIn(true);
    }

    xmlHttp.open("POST", `http://${window.location.host}/php/signIn.php`);

    const rq = JSON.stringify({
      "username": document.getElementById("username").value,
      "password": document.getElementById("password").value
    })

    xmlHttp.send(rq);
  }

  if(loggedIn) 
    return <Redirect to={{
      path: "/",
      state: {"user": document.getElementById("username").value}
    }
  } />


  return (
    <div className="ThoughtPage">
      <div className="CenteredDiv">
        <img className="CircleDragon" src="dragonCircle.png" />
        <div className="InputForm">
          <input type="text" id="username" />
          <input type="password" id="password" />
          <button onClick={signInRq}> Sign in </button>
        </div>
      </div>
    </div>
  )
}
