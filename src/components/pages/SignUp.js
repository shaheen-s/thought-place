import React, { useState } from "react";

export default (props) => {
  return (
    <div className="ThoughtPage">
      <form className="CenteredDiv" action="../../php/signUp.php" method="post">
        <img className="CircleDragon" src="dragonCircle.png" />
        <div className="InputForm">
          <input type="text" placeholder="username " name="username " />
          <input type="password" placeholder="password" name="password" />
          <input type="password" placeholder="repeatPassword" name="repeatPassword" />
          <input type="submit" value="Sign up!" />
        </div>
      </form>
    </div>
  )
}
