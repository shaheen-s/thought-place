import React, {useState} from "react";
import { Redirect } from "react-router-dom"
import ThoughtProducer from "./components/ThoughtProducer"
import ThoughtDisplay from "./components/ThoughtDisplay"



export default (props) => {
  const authorized = props.location && props.location.state && props.location.state.user?props.location.state.user : false;
  if(!authorized) return <Redirect to="/signin" />;
  const [thoughts, updateThoughts] = useState([]);
  const [callThoughts, updateCallThoughts] = useState(true);

  const getThoughts = () => {
    const xmlHttp = new XMLHttpRequest();
    xmlHttp.onload = function (param1, param2) {
      const rs = JSON.parse(this.response);
      if(rs) updateThoughts(rs);
      updateCallThoughts(false)
    }
    xmlHttp.onerror = () => {updateCallThoughts(false)}
    xmlHttp.open("GET", "http://localhost:8000/php/getThoughts.php");
    xmlHttp.send();
  }

  if(callThoughts) getThoughts();

  return (
    <div className="ThoughtPage">
      <button onClick={() => updateCallThoughts(true)}> Refresh Tweets </button>
      <ThoughtProducer user={authorized} />
      <ThoughtDisplay thoughts={thoughts} />
    </div>
  )



}
